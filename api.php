......
......

 /**
     *  SearchProducts API for iOS APP, Web APP, etc.
     *  get all filter parameters by GET or POST
     *  @search conditions details :
     *      query : query string
     *      category : category id
     *      page_offset : page offset , start from 1 , default is 1
     *      page_limit : page products limitation default 50
     *      gender : search gender id which is from df_lu_genders table default is 2 (female)
     *      color : search color id which is from df_lu_color table
     *      brand : search brand id which is from df_lu_brands table
     *      store : search store id which is from df_lu_advertisers table
     *      sort_type : sort type flag with values ( 1 -> sort by price , 2 -> most discount for more soon), default no sort
     *      sort_dir : sort flag with values ( 'ASC' -> LOWEST TO HIGHEST PRICE, 'DESC' -> HIGHEST TO LOWEST PRICE), default as 'ASC'
     *      left_price : search for from price , default is 0
     *      right_price : search for end price , default is 0
     *      country_code : search for products in specified country code which is from iso3 FIELD of country table, default is 'AUS'
     *      product_id : just search specified product information
     *
     *  @return
     *     return all with json format data
     *     search ok return :
     *      {
     *           'success' : 1,
     *          'results' : {
     *               total : the number of search results
     *               pages : the page number of search results
     *               s_min_price : min price for search results
     *               s_max_price :  max price for search results
     *               colors : [{id,title}...], all colors for search results
     *               genders : [{id_gender,gender}...], all genders for search results
     *               stores : [{id_store,name,include,exclude}...], all stores for search results
     *               categories:{ categoryid:{id,name}}
     *               products : [] all products information
     *              }
     *       }
     *
     *     search failed return:
     *      {
     *           'success' : 0,
     *           'error' : error description
     *          'query_conditions' : {
     *              it is your passed in query conditions
     *              }
     *       }
     */
    private function searchProducts()
    {
        header('Content-Type: application/json');
        // make it easy to support GET or POST
        $method_tag = $_REQUEST;

        // make up all search conditions
        $search_conditions = array();

        // get query string
        if ( isset($method_tag['query']) ) {
            $search_conditions['search'] = $method_tag['query'];
        }

        // get filter : category
        if ( isset($method_tag['category']) ) {
            $search_conditions['category'] = (int)$method_tag['category'];
        }

        // get page offset and limit
        if ( isset($method_tag['page_offset']) ) {
            $search_conditions['page_offset'] = $method_tag['page_offset'];
        } else {
            $search_conditions['page_offset'] = 1;
        }
        if ( isset($method_tag['page_limit']) ) {
            $search_conditions['page_limit'] = $method_tag['page_limit'];
        }

        // Gender
        if ( !empty($method_tag['gender']) ) {
            $search_conditions['gender'] = (int)$method_tag['gender'];
        }
        if ( isset($method_tag['color']) ) {
            $search_conditions['color'] = $method_tag['color'];
        }
        if ( isset($method_tag['brand']) ) {
            $search_conditions['brand_id'] = $method_tag['brand'];
        }
        if ( isset($method_tag['store']) ) {
            $search_conditions['advertiser'] = $method_tag['store'];
        }


        $search_conditions['sort'] = -1;
        if ( isset($method_tag['sort_type']) ) {
            $sort_index = (int)$method_tag['sort_type'];
            switch ($sort_index) {
                case 1:{ // sort by price
                    $sort_dir = 'ASC';
                    if ( isset($method_tag['sort_dir']) ) {
                        $sort_dir = trim($method_tag['sort_dir']);
                    }
                    $sort_dir = strtoupper($sort_dir);
                    if ( $sort_dir == 'ASC' ) {
                        $search_conditions['sort'] = 1;
                    } else if ( $sort_dir === 'DESC' ) {
                        $search_conditions['sort'] = 2;
                    }
                }
                    break;

                case 2:{ // sort by most discount
                    $search_conditions['sort'] = 3;
                }
                    break;
            }
        }

        $min_price = 0;
        $max_price = 0;
        if ( isset($method_tag['left_price']) ) {
            $min_price = $method_tag['left_price'];
        }
        if ( isset($method_tag['right_price']) ) {
            $max_price = $method_tag['right_price'];
        }
        $search_conditions['min_price'] = $min_price;
        $search_conditions['max_price'] = $max_price;

        // search by country , set default as australia
        // the country code is from iso field of country table
        //$search_conditions['country_code'] = 'AUS';
        if ( isset($method_tag['country_code']) ) {
            $search_conditions['country_code'] = $method_tag['country_code'];
        } else {
            $search_conditions['country_code'] = '000'; // default for global
        }

        $json = array();
        $json['success'] = 1;
        // get all relative product ids
        if ( isset($search_conditions['search']) && !empty($search_conditions['search']) ) {
            $prod_ids = $this->api_model->getProductIdsBySolr($search_conditions);
            if ( isset($prod_ids) && count($prod_ids) > 0 ) {
                $search_conditions['prod_ids'] = implode('+',$prod_ids);
            }
        }

        // check to see if only search specified product information only
        if ( isset($method_tag['product_id']) ) {
            $search_conditions['product_id'] = $method_tag['product_id'];
        }

        $find_products = $this->api_model->findProductsBySolr($search_conditions);
        if ( !isset($find_products) || count($find_products['results']) <= 0 ) {
            $json['success'] = 0;
            $json['error'] = 'matched nothing';
            $json['query_conditions'] = $search_conditions;
        } else {
            $data['products'] = $find_products['results'];
            $data['pages'] = $find_products['pages'];
            $data['brands'] = $find_products['brands'];
            $data['stores'] = $find_products['advertisers'];
            $data['categories'] = $find_products['cats'];
            $data['total'] = $find_products['total'];
            $data['colors'] = $find_products['colors'];
            $data['genders'] = $find_products['genders'];
            $data['s_min_price'] = 0;
            $data['s_max_price'] = 2000;
            if (isset($find_products['min_price'])) {
                $data['s_min_price'] = $find_products['min_price'];
            }
            if (isset($find_products['max_price'])) {
                $data['s_max_price'] = $find_products['max_price'];
            }

            // covert price to current country currency
            // and make up image url
            $this->load->model('country/country_model');
            $country_current_currency = $this->country_model->userCountryCodeToCurrency($search_conditions['country_code']);
            $symbol = $this->country_model->userCurrencySymbol($country_current_currency);

            // convert for s_min_price and s_max_price
            $rate = $this->country_model->GetCCR('AUD', $country_current_currency);
            $data['s_min_price'] = round($data['s_min_price']*$rate, 2);
            $data['s_max_price'] = round($data['s_max_price']*$rate, 2);

            foreach ($data['products'] as $k => $v) {

                // make up orginal price
                $val_org = $v['price'];
                $cur = 'AUD';
                if (isset($v['currency']) && !empty($v['currency'])) {
                    $cur = $v['currency'];
                }
                $rate = $this->country_model->GetCCR($cur, $country_current_currency);
                $figure = $val_org * $rate;
                $price = round($figure, 2);
                $v['price'] = $symbol . $price . ' ' . $country_current_currency;

                // makeup sale price if existing
                if (isset($v['salePrice']) && $v['salePrice'] > 0) {
                    $val_sale = $v['salePrice'];

                    $figure = $val_sale * $this->country_model->GetCCR($cur, $country_current_currency);
                    $price = round($figure, 2);
                    $v['salePrice'] = $symbol . $price . ' ' . $country_current_currency;
                    if ($val_sale >= $val_org) {
                        // if sale price greater than original price, we just show sale price only
                        $v['price'] = $v['salePrice'];
                        unset($v['salePrice']);
                    } else {
                        $percent_off = (($val_org - $val_sale) / $val_org) * 100;
                        $v['discount'] = number_format($percent_off, 2, '.', '') . "%";
                    }
                }
                $data['products'][$k] = $v;
            }
            $json['results'] = $data;
        }
        
        echo json_encode($json);

    }

    /**
     * @param $actionId
     * @param $receiver
     * @param $sender
     * @param $email_content
     *      include body and subject
     */

    private function getEmailContent($actionId,$pushInfo,$receiver,$sender,&$email_content)
    {
        /*
       1.$sedner started following you.
       2.$sender clicked your product - link to closet image foenix://product?id=closet_id
       3.$sender liked your product - link to closet image foenix://closet?id=closet_id
       4.$sender mentioned you in comment - link to foenix://comments?id=closet_id
       7. $sender has sent you a friend request - link to foenix://requests
       10. $sender tagged products in your photo - foenix://approvetags
       11. $sender tagged you in an image - foenix://closet?id=closet_id
       13. $sender commented on your photo - foenix://comments?id=closet_id
       14. $sender accepted your friend request - foenix://profile?id=sender_id
       16. $sender approved your tag - foenix://closet?id=closet_id
       */
        //$base_url = base_url();
        $base_url = 'http://foenix.co/';

        switch ($actionId)  {

            // for action 1
            // somebody follow you
            // 1.$sedner started following you.
            case 1: {
                $profile_image = $base_url . 'mobile/profileimages/' . $sender['image'];
                $sender_name = $sender['handle'];
                $user_profile_url = $base_url . 'users/pushlink?actid=' . $actionId .'&userid=' . $sender['id'];
                $email_content['body'] = str_replace(
                    array('{{PROFILE_IMAGE}}', '{{USERNAME}}','{{USERNAME_U}}','{{USER_URL}}'),
                    array($profile_image, $sender_name, strtoupper($sender_name), $user_profile_url), $email_content['body']);
                $email_content['subject'] = str_replace(
                    array('{{USERNAME}}'),
                    array($sender_name), $email_content['subject']);
            }
                break;

            // action 2
            // 2.$sender clicked your product - link to closet image foenix://closet?id=closet_id
            case 2: {

                $profile_image = $base_url . 'mobile/profileimages/' . $sender['image'];
                $sender_name = $sender['handle'];
                $user_profile_url = $base_url . 'users/pushlink?actid=' . $actionId .'&userid=' . $sender['id'];
                $my_closet_url = $base_url . 'users/pushlink?actid=' . $actionId .'&closetid=' . $pushInfo['closet_id'] . '&prodid=' . $pushInfo['product_id'];

                // get product information
                $this->load->model('search/search_model');
                $product_info = $this->search_model->getProductBasicInfoFromSorl($pushInfo['product_id']);
                $product_ttl = "";
                $product_image = "";
                if ( isset($product_info) ) {
                    $product_ttl = $product_info['title'];
                    $product_image = $product_info['imageUrl'];
                }
                $email_content['subject'] = str_replace(
                    array('{{USERNAME}}'),
                    array($sender_name), $email_content['subject']);
                $email_content['body'] = str_replace(
                    array('{{PROFILE_IMAGE}}', '{{USERNAME}}','{{USERNAME_U}}','{{USER_URL}}','{{PRODUCT_TTL}}','{{PRODUCT_IMAGE}}','{{CLOSET_URL}}'),
                    array($profile_image, $sender_name,strtoupper($sender_name),$user_profile_url,$product_ttl,$product_image,$my_closet_url), $email_content['body']);

            }
                break;

            // action 3
            // 3.$sender liked your product - link to closet image foenix://closet?id=closet_id
            case 3: {

                $profile_image = $base_url. 'mobile/profileimages/' . $sender['image'];
                $sender_name = $sender['handle'];
                $user_profile_url = $base_url  . 'users/pushlink?actid=' . $actionId .'&userid=' . $sender['id'];
                $my_closet_url = $base_url . 'users/pushlink?actid=' . $actionId .'&closetid=' . $pushInfo['closet_id'];

                // get closet image
                $this->load->model('thecloset/thecloset_model');
                $closet_info = $this->thecloset_model->getClosetInfo($pushInfo['closet_id']);
                if (filter_var($closet_info['image'], FILTER_VALIDATE_URL) === FALSE) {
                    $closet_image = base_url() . 'mobile/closetimages/' . $closet_info['image'];
                } else {
                    $closet_image = $closet_info['image'];
                }

                $email_content['subject'] = str_replace(
                    array('{{USERNAME}}'),
                    array($sender_name), $email_content['subject']);

                $email_content['body'] = str_replace(
                    array('{{PROFILE_IMAGE}}', '{{USERNAME}}','{{USERNAME_U}}','{{USER_URL}}','{{CLOSET_IMAGE}}','{{CLOSET_URL}}'),
                    array($profile_image, $sender_name,strtoupper($sender_name),$user_profile_url,$closet_image,$my_closet_url), $email_content['body']);


            }
                break;


            // action 4
            // 4.$sender mentioned you in comment - link to foenix://comments?id=closet_id
            case 4: {

                $profile_image = $base_url . 'mobile/profileimages/' . $sender['image'];
                $sender_name = $sender['handle'];
                $user_profile_url = $base_url . 'users/pushlink?actid=' . $actionId .'&userid=' . $sender['id'];
                $comment_url = $base_url . 'users/pushlink?actid=' . $actionId .'&closetid=' . $pushInfo['closet_id'];

                $email_content['subject'] = str_replace(
                    array('{{USERNAME}}'),
                    array($sender_name), $email_content['subject']);
                $email_content['body'] = str_replace(
                    array('{{PROFILE_IMAGE}}', '{{USER_URL}}' ,'{{USERNAME}}','{{COMMENT_URL}}'),
                    array($profile_image, $user_profile_url,$sender_name,$comment_url), $email_content['body']);

            }
                break;

            // action 7
            // 7. $sender has sent you a friend request - link to foenix://requests
            case 7:{
                $profile_image = $base_url . 'mobile/profileimages/' . $sender['image'];
                $sender_name = $sender['handle'];
                $user_profile_url = $base_url . 'users/pushlink?actid=' . $actionId .'&userid=' . $sender['id'];

                $email_content['subject'] = str_replace(
                    array('{{USERNAME}}'),
                    array($sender_name), $email_content['subject']);
                $email_content['body'] = str_replace(
                    array('{{PROFILE_IMAGE}}','{{USERNAME}}','{{USERNAME_U}}','{{USER_URL}}'),
                    array($profile_image, $sender_name,strtoupper($sender_name),$user_profile_url), $email_content['body']);
            }
                break;

            // action 10
            // 10. $sender tagged products in your photo - foenix://approvetags
            case 10:{
                $profile_image = $base_url . 'mobile/profileimages/' . $sender['image'];
                $sender_name = $sender['handle'];
                $user_profile_url = $base_url . 'users/pushlink?actid=' . $actionId .'&userid=' . $sender['id'];
                $my_closet_url = $base_url . 'users/pushlink?actid=' . $actionId .'&closetid=' . $pushInfo['closet_id'];

                // get closet image
                $this->load->model('thecloset/thecloset_model');
                $closet_info = $this->thecloset_model->getClosetInfo($pushInfo['closet_id']);
                if (filter_var($closet_info['image'], FILTER_VALIDATE_URL) === FALSE) {
                    $closet_image = base_url() . 'mobile/closetimages/' . $closet_info['image'];
                } else {
                    $closet_image = $closet_info['image'];
                }

                $email_content['subject'] = str_replace(
                    array('{{USERNAME}}'),
                    array($sender_name), $email_content['subject']);
                $email_content['body'] = str_replace(
                    array('{{PROFILE_IMAGE}}','{{USERNAME}}','{{USERNAME_U}}','{{USER_URL}}','{{CLOSET_IMAGE}}','{{CLOSET_URL}}'),
                    array($profile_image, $sender_name,strtoupper($sender_name),$user_profile_url,$closet_image,$my_closet_url), $email_content['body']);
            }
                break;

            // action 11
            // 11. $sender tagged you in an image - foenix://closet?id=closet_id
            case 11:{
                $profile_image = $base_url . 'mobile/profileimages/' . $sender['image'];
                $sender_name = $sender['handle'];
                $user_profile_url = $base_url . 'users/pushlink?actid=' . $actionId .'&userid=' . $sender['id'];
                $my_closet_url =  $base_url. 'users/pushlink?actid=' . $actionId .'&closetid=' . $pushInfo['closet_id'];

                // get closet image
                $this->load->model('thecloset/thecloset_model');
                $closet_info = $this->thecloset_model->getClosetInfo($pushInfo['closet_id']);
                if (filter_var($closet_info['image'], FILTER_VALIDATE_URL) === FALSE) {
                    $closet_image = $base_url . 'mobile/closetimages/' . $closet_info['image'];
                } else {
                    $closet_image = $closet_info['image'];
                }

                $email_content['subject'] = str_replace(
                    array('{{USERNAME}}'),
                    array($sender_name), $email_content['subject']);
                $email_content['body'] = str_replace(
                    array('{{PROFILE_IMAGE}}','{{USERNAME}}','{{USERNAME_U}}','{{USER_URL}}','{{CLOSET_IMAGE}}','{{CLOSET_URL}}'),
                    array($profile_image, $sender_name,strtoupper($sender_name),$user_profile_url,$closet_image,$my_closet_url), $email_content['body']);
            }
                break;

            // 13. $sender commented on your photo - foenix://comments?id=closet_id
            case 13:
            {
                $profile_image = $base_url . 'mobile/profileimages/' . $sender['image'];
                $sender_name = $sender['handle'];
                $user_profile_url = $base_url . 'users/pushlink?actid=' . $actionId .'&userid=' . $sender['id'];
                $my_closet_url = $base_url . 'users/pushlink?actid=' . $actionId .'&closetid=' . $pushInfo['closet_id'];

                // get closet image
                $this->load->model('thecloset/thecloset_model');
                $closet_info = $this->thecloset_model->getClosetInfo($pushInfo['closet_id']);
                if (filter_var($closet_info['image'], FILTER_VALIDATE_URL) === FALSE) {
                    $closet_image = $base_url . 'mobile/closetimages/' . $closet_info['image'];
                } else {
                    $closet_image = $closet_info['image'];
                }
                $email_content['subject'] = str_replace(
                    array('{{USERNAME}}'),
                    array($sender_name), $email_content['subject']);

                $email_content['body'] = str_replace(
                    array('{{PROFILE_IMAGE}}','{{USERNAME}}','{{USERNAME_U}}','{{USER_URL}}','{{CLOSET_IMAGE}}','{{CLOSET_URL}}'),
                    array($profile_image, $sender_name,strtoupper($sender_name),$user_profile_url,$closet_image,$my_closet_url), $email_content['body']);

            }
                break;


            // 14. $sender accepted your friend request - foenix://profile?id=sender_id
            case 14:
            {
                $profile_image = $base_url . 'mobile/profileimages/' . $sender['image'];
                $sender_name = $sender['handle'];
                $user_profile_url = $base_url . 'users/pushlink?actid=' . $actionId .'&userid=' . $sender['id'];


                $email_content['subject'] = str_replace(
                    array('{{USERNAME}}'),
                    array($sender_name), $email_content['subject']);
                $email_content['body'] = str_replace(
                    array('{{PROFILE_IMAGE}}','{{USERNAME}}','{{USERNAME_U}}','{{USER_URL}}'),
                    array($profile_image, $sender_name,strtoupper($sender_name),$user_profile_url), $email_content['body']);

            }
                break;


            // 16. $sender approved your tag - foenix://closet?id=closet_id
            case 16:
            {
                $profile_image = $base_url . 'mobile/profileimages/' . $sender['image'];
                $sender_name = $sender['handle'];
                $user_profile_url = $base_url . 'users/pushlink?actid=' . $actionId .'&userid=' . $sender['id'];
                $my_closet_url = $base_url . 'users/pushlink?actid=' . $actionId .'&closetid=' . $pushInfo['closet_id'];

                // get closet image
                $this->load->model('thecloset/thecloset_model');
                $closet_info = $this->thecloset_model->getClosetInfo($pushInfo['closet_id']);
                if (filter_var($closet_info['image'], FILTER_VALIDATE_URL) === FALSE) {
                    $closet_image = $base_url . 'mobile/closetimages/' . $closet_info['image'];
                } else {
                    $closet_image = $closet_info['image'];
                }

                $email_content['subject'] = str_replace(
                    array('{{USERNAME}}'),
                    array($sender_name), $email_content['subject']);
                $email_content['body'] = str_replace(
                    array('{{PROFILE_IMAGE}}','{{USERNAME}}','{{USERNAME_U}}','{{USER_URL}}','{{CLOSET_IMAGE}}','{{CLOSET_URL}}'),
                    array($profile_image, $sender_name,strtoupper($sender_name),$user_profile_url,$closet_image,$my_closet_url), $email_content['body']);

            }
                break;

        }
    }


    /**
     * should be called by client
     * client need post parameters as well
     * currently only get action ID from client
     * then based on action ID to do related things
     * for example :
     *    sending email
     *    or push notifications to Apple push server
     */
    private function triggerPushEvent(){

        $method_tag = $_REQUEST;

        $push_index = -1;
