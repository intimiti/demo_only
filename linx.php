.....

 /*
     * modify publisher payment
     * will be called from front-end by ajax
     */
    public function  publisherModPayment()
    {
        $data = array();
        $data['error'] = array();

        $user_id = 0;
        if (isset ($_POST['userid'])) {
            $user_id = $_POST['userid'];
        }

        $pay_method = 1;
        if (isset ($_POST['pay_method'])) {
            $pay_method = $_POST['pay_method'];
        }

        $pay_name = "";
        if (isset ($_POST['pay_name'])) {
            $pay_name = $_POST['pay_name'];
        }

        $pay_number = "";
        if (isset ($_POST['pay_number'])) {
            $pay_number = $_POST['pay_number'];
        }

        $pay_bsb = "000-000";
        if (isset ($_POST['bsb_head']) && isset($_POST['bsb_tail'])) {
            $pay_bsb = $_POST['bsb_head'] . '-' . $_POST['bsb_tail'];
        }

        $pay_schedule = 1;
        if (isset ($_POST['pay_schedule'])) {
            $pay_schedule = $_POST['pay_schedule'];
        }

        if ( empty($data['error']) ) {
            $data['success'] = true;

            $update_data = array(
                'pay_method' => $pay_method,
                'pay_number' => $pay_number,
                'pay_name' => $pay_name,
                'pay_bsb' => $pay_bsb,
                'pay_schedule' => $pay_schedule,
            );

            // update database as well
            $this->db->where('user_id',$user_id);
            $this->db->update('linx_publisher',$update_data);



        }

        echo json_encode($data);

    }

    /*
     * modify advertiser commission rate
     * will be called from front-end by ajax
     */
    public function advertiserModComm(){
        $data = array();
        $data['error'] = array();

        $user_id = 0;
        if (isset ($_POST['userid'])) {
            $user_id = $_POST['userid'];
        }

        $comm_rate = 0;
        if (isset ($_POST['comm'])) {
            $comm_rate = intval($_POST['comm']);
        }
        if ( $comm_rate <= 0 ||  $comm_rate > 100 ) {
            $data['error']['comm'] = 'The commission rate must be between 1 to 100';
            echo json_encode($data);
            return;
        }

        $confirm_comm_rate = 0;
        if (isset ($_POST['confirm_comm'])) {
            $confirm_comm_rate = intval($_POST['confirm_comm']);
        }
        if ( $comm_rate != $confirm_comm_rate ) {
            $data['error']['confirm_comm'] = 'Both commission are not matched';
            echo json_encode($data);
            return;
        }

        if ( empty($data['error']) ) {
            $data['success'] = true;
            $data['comm'] = $comm_rate;

            $update_data = array(
                'ratio' => $comm_rate
            );

            // update database as well
            $this->db->where('user_id',$user_id);
            $this->db->update('linx_advertiser',$update_data);

        }

        echo json_encode($data);
    }

    public function storeModServiceCountry(){
        $data = array();
        $data['error'] = array();

        $store_id = 0;
        if (isset ($_POST['store_id'])) {
            $store_id = $_POST['store_id'];
        }

        $country_str = "";
        if (isset ($_POST['service_country'])) {
            $country_str = $_POST['service_country'];
            $country_str = rtrim($country_str,',');
        }


        if ( empty($data['error']) ) {
            $data['success'] = true;

            // check to see if it is global
            if ( $country_str == "GGG" ) {
                $country_str = "";
            }
            $update_data = array(
                'include' => $country_str
            );

            $data['countries'] =  $country_str;

            // update database as well
            $this->db->where('adv_id',$store_id);
            $this->db->update('df_lu_advertisers',$update_data);

        }

        echo json_encode($data);
    }

    /*
     * modify advertiser service country
     * will be called from front-end by ajax
     */
    public function advertiserModServiceCountry() {
        $data = array();
        $data['error'] = array();

        $user_id = 0;
        if (isset ($_POST['userid'])) {
            $user_id = $_POST['userid'];
        }

        $service_country = array();
        if (isset ($_POST['service_country'])) {
            $country_str = $_POST['service_country'];
            $country_str = rtrim($country_str,"|");
            $service_country = explode('|',$country_str);
        }


        if ( empty($data['error']) ) {
            $data['success'] = true;

            $update_data = array(
                'service_country' => serialize($service_country)
            );

            // update database as well
            $this->db->where('user_id',$user_id);
            $this->db->update('linx_advertiser',$update_data);

        }

        echo json_encode($data);
    }

    /*
     * modify advertiser program terms
     * will be called from front-end by ajax
     */
    public function  advertiserModTerms()
    {
        $data = array();
        $data['error'] = array();

        $user_id = 0;
        if (isset ($_POST['userid'])) {
            $user_id = $_POST['userid'];
        }

        $policy_selected = array();
        if (isset ($_POST['policy'])) {
            $policy_selected = $_POST['policy'];
        }

        $restrict_keys = "";
        if ( isset($_POST['restrict_keys']) ){
            $restrict_keys = $_POST['restrict_keys'];
        }
        $data['policy'] = $policy_selected;

        if ( empty($data['error']) ) {
            $data['success'] = true;

            $update_data = array(
                'policy' => serialize($policy_selected),
                'restrict_keys' => $restrict_keys
            );

            // update database as well
            $this->db->where('user_id',$user_id);
            $this->db->update('linx_advertiser',$update_data);

        }

        echo json_encode($data);

    }


    /*
     * modify publisher traffic source channels
     * will be called from front-end by ajax
     */
    public function  publisherModChannels()
    {
        $data = array();
        $data['error'] = array();

        $user_id = 0;
        if (isset ($_POST['userid'])) {
            $user_id = $_POST['userid'];
        }

        $traffic_selected = array();
        if (isset ($_POST['traffic_source'])) {
            $traffic_selected = $_POST['traffic_source'];
        }

        $data['traffic_source'] = $traffic_selected;

        if ( empty($data['error']) ) {
            $data['success'] = true;

            $update_data = array(
                'traffic_source' => serialize($traffic_selected)
            );

            // update database as well
            $this->db->where('user_id',$user_id);
            $this->db->update('linx_publisher',$update_data);

        }

        echo json_encode($data);

    }
